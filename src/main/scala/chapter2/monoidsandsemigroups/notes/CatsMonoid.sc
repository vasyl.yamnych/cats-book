import cats.Monoid
import cats.instances.string._ // for Monoid

Monoid[String].combine("Hi ", "there")
Monoid[String].empty

Monoid.apply[String].combine("Hi ", "there")
Monoid.apply[String].empty

import cats.Semigroup
Semigroup[String].combine("Hi ", "there")

import cats.instances.int._ // for Monoid
Monoid[Int].combine(32, 10)

import cats.instances.option._ // for Monoid
val a = Option(22)
val b = Option(20)
Monoid[Option[Int]].combine(a, b)

import cats.syntax.semigroup._ // for |+|

val stringResult = "Hi " |+| "there" |+| Monoid[String].empty
val intResult = 1 |+| 2 |+| Monoid[Int].empty

"Scala" |+| " with " |+| "Cats"
Option(1) |+| Option(2)

import cats.instances.map._  // for Monoid
val map1 = Map("a" -> 1, "b" -> 2)
val map2 = Map("b" -> 3, "d" -> 4)
map1 |+| map2

import cats.instances.tuple._ // for Monoid
val tuple1 = ("hello", 123)
val tuple2 = ("world", 321)
tuple1 |+| tuple2

def addAll[A](values: List[A])(implicit monoid: Monoid[A]): A =
  values.foldRight(monoid.empty)(_ |+| _)

addAll(List(1, 2, 3))
addAll(List(None, Some(1), Some(2)))