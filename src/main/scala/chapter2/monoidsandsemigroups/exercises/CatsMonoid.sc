def add(items: List[Int]): Int = items.foldLeft(0)(_ + _)

import cats.Monoid
import cats.instances.int._ // for Monoid
import cats.instances.option._ // for Monoid
import cats.syntax.semigroup._ // for |+|

//def add(items: List[Int]): Int = items.foldLeft(Monoid[Int].empty)(_ |+| _)

def add[A](items: List[A])(implicit monoid: Monoid[A]): A =
  items.foldLeft(monoid.empty)(_ |+| _)

def add2[A: Monoid](items: List[A]): A =
  items.foldLeft(Monoid[A].empty)(_ |+| _)

add(List(1, 2, 3))
add2(List(1, 2, 3))

add(List(Some(1), None, Option(2), Some(3)))
add2(List(Some(1), None, Option(2), Some(3)))

//add(List(Some(1), Some(2), Some(3)))

case class Order(totalCost: Double, quantity: Double)

import cats.instances.double._ // for Monoid

implicit val orderMonoid = new Monoid[Order] {
  def combine(x: Order, y: Order): Order =
    Order(x.totalCost |+| y.totalCost, x.quantity |+| y.quantity)
  def empty: Order = Order(0, 0)
}

add(List(Order(1, 1.1), Order(2, 2.2), Order(3, 3.3)))