final case class GCounter0(counters: Map[String, Int]) {
  def increment(machine: String, amount: Int): GCounter0 = {
    val value = amount + counters.getOrElse(machine, 0)
    GCounter0(counters + (machine -> value))
  }
  def merge(that: GCounter0): GCounter0 =
    GCounter0(that.counters ++ this.counters.map {
      case (k, v) =>
        k -> (v max that.counters.getOrElse(k, 0))
    })
  def total: Int = counters.values.sum
}

import cats.kernel.CommutativeMonoid
trait BoundedSemiLattice[A] extends CommutativeMonoid[A] {
  def combine(a1: A, a2: A): A
  def empty: A
}

import cats.instances.list._ // for Monoid
import cats.instances.map._ // for Monoid
import cats.syntax.semigroup._ // for |+|
import cats.syntax.foldable._ // for combineAll
final case class GCounter[A](counters: Map[String, A]) {
  def increment(machine: String, amount: A)(implicit m: CommutativeMonoid[A]): GCounter[A] = {
    val value = amount |+| counters.getOrElse(machine, m.empty)
    GCounter(counters + (machine -> value))
  }
  def merge(that: GCounter[A])(implicit b: BoundedSemiLattice[A]): GCounter[A] =
    GCounter(this.counters |+| that.counters)
  def total(implicit m: CommutativeMonoid[A]): A =
    this.counters.values.toList.combineAll
}
