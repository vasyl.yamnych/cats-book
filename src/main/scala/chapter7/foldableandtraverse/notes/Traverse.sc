import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

val hostnames =
    List("alpha.example.com", "beta.example.com", "gamma.demo.com")
def getUptime(hostname: String): Future[Int] =
    Future(hostname.length * 60)

val allUptimes: Future[List[Int]] =
    hostnames.foldLeft(Future(List.empty[Int])) { (accum, host) =>
      val uptime = getUptime(host)
      for {
        accum <- accum
        uptime <- uptime
      } yield accum :+ uptime
    }
Await.result(allUptimes, 1.second)

val allUptimes2: Future[List[Int]] =
    Future.traverse(hostnames)(getUptime)
Await.result(allUptimes2, 1.second)

import cats.Applicative
import cats.instances.future._ // for Applicative
import cats.syntax.applicative._ // for pure
List.empty[Int].pure[Future]

def oldCombine(
      accum: Future[List[Int]],
      host: String
  ): Future[List[Int]] = {
    val uptime = getUptime(host)
    for {
      accum <- accum
      uptime <- uptime
    } yield accum :+ uptime
  }

import cats.syntax.apply._ // for mapN
// Combining accumulator and hostname using an Applicative:

def newCombine(accum: Future[List[Int]], host: String): Future[List[Int]] =
    (accum, getUptime(host)).mapN(_ :+ _)

import scala.language.higherKinds

def listTraverse[F[_]: Applicative, A, B](list: List[A])(
      func: A => F[B]): F[List[B]] =
    list.foldLeft(List.empty[B].pure[F]) { (accum, item) =>
      (accum, func(item)).mapN(_ :+ _)
    }

def listSequence[F[_]: Applicative, B](list: List[F[B]]): F[List[B]] =
    listTraverse(list)(identity)

val totalUptime = listTraverse(hostnames)(getUptime)
Await.result(totalUptime, 1.second)

import cats.instances.vector._ // for Applicative
listSequence(List(Vector(1, 2), Vector(3, 4)))
listSequence(List(Vector(1, 2), Vector(3, 4), Vector(5, 6)))

import cats.instances.option._ // for Applicative

def process(inputs: List[Int]): Option[List[Int]] =
    listTraverse(inputs)(n => if (n % 2 == 0) Some(n) else None)

process(List(2, 4, 6))
process(List(1, 2, 3))

import cats.data.Validated
import cats.instances.list._ // for Monoid
  type ErrorsOr[A] = Validated[List[String], A]
def process2(inputs: List[Int]): ErrorsOr[List[Int]] =
    listTraverse(inputs) { n =>
      if (n % 2 == 0) {
        Validated.valid(n)
      } else {
        Validated.invalid(List(s"$n is not even"))
      }
    }

process2(List(2, 4, 6))
process2(List(1, 2, 3))

import cats.Traverse
import cats.instances.future._ // for Applicative
import cats.instances.list._ // for Traverse

val totalUptime2: Future[List[Int]] =
    Traverse[List].traverse(hostnames)(getUptime)
Await.result(totalUptime2, 1.second)

val numbers = List(Future(1), Future(2), Future(3))
val numbers2: Future[List[Int]] = Traverse[List].sequence(numbers)
Await.result(numbers2, 1.second)

import cats.syntax.traverse._ // for sequence and traverse

Await.result(hostnames.traverse(getUptime), 1.second)
Await.result(numbers.sequence, 1.second)