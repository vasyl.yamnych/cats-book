List(1, 2, 3).foldLeft(0)(_ + _)
List(1, 2, 3).foldRight(0)(_ + _)

List(1, 2, 3).foldLeft(0)(_ - _)
List(1, 2, 3).foldRight(0)(_ - _)

List(1, 2, 3).foldLeft(List.empty[Int])((acc, v) => v :: acc)
List(1, 2, 3).foldRight(List.empty[Int])((v, acc) => v :: acc)

def map[A, B](list: List[A])(f: A => B): List[B] =
    list.foldRight(List.empty[B])((v, acc) => f(v) :: acc)

map(List(1, 2, 3))(_ * 2)

def flatMap[A, B](list: List[A])(f: A => List[B]): List[B] =
    list.foldRight(List.empty[B])((v, acc) => f(v) ::: acc)

flatMap(List(1, 2, 3))(a => List(a, a * 10, a * 100))

def filter[A](list: List[A])(f: A => Boolean): List[A] =
    list.foldRight(List.empty[A])((v, acc) => if (f(v)) v :: acc else acc)

filter(List(1, 2, 3))(_ % 2 == 1)

import scala.math.Numeric
def sumWithNumeric[A](list: List[A])(implicit numeric: Numeric[A]): A =
    list.foldRight(numeric.zero)(numeric.plus)
sumWithNumeric(List(1, 2, 3))

import cats.Monoid
def sumWithMonoid[A](list: List[A])(implicit monoid: Monoid[A]): A =
    list.foldRight(monoid.empty)(monoid.combine)
import cats.instances.int._ // for Monoid
sumWithMonoid(List(1, 2, 3))

import cats.Foldable
import cats.instances.list._ // for Foldable
val ints = List(1, 2, 3)
Foldable[List].foldLeft(ints, 0)(_ + _)

import cats.instances.option._ // for Foldable
val maybeInt = Option(123)
Foldable[Option].foldLeft(maybeInt, 10)(_ * _)

import cats.Eval
import cats.Foldable
def bigData = (1 to 100000).toStream
//bigData.foldRight(0L)(_ + _) // it is not stack safe

import cats.instances.stream._ // for Foldable
val eval: Eval[Long] =
    Foldable[Stream].
      foldRight(bigData, Eval.now(0L)) { (num, eval) =>
          eval.map(_ + num)
      }
eval.value

(1 to 100000).toList.foldRight(0L)(_ + _)
(1 to 100000).toVector.foldRight(0L)(_ + _)

Foldable[Option].nonEmpty(Option(42))
Foldable[List].find(List(1, 2, 3))(_ % 2 == 0)

import cats.instances.int._ // for Monoid
Foldable[List].combineAll(List(1, 2, 3))

import cats.instances.string._ // for Monoid
Foldable[List].foldMap(List(1, 2, 3))(_.toString)

import cats.instances.vector._ // for Monoid

val ints1 = List(Vector(1, 2, 3), Vector(4, 5, 6))
(Foldable[List] compose Foldable[Vector]).combineAll(ints1)

import cats.syntax.foldable._ // for combineAll and foldMap
List(1, 2, 3).combineAll
List(1, 2, 3).foldMap(_.toString)