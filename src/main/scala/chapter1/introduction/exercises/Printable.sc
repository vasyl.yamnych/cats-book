trait Printable[A] {
  def format(input: A): String
}

object PrintableInstances {
  implicit val stringPrintable: Printable[String] = new Printable[String] {
    def format(input: String): String = input
  }
  implicit val intPrintable: Printable[Int] = new Printable[Int] {
    def format(input: Int): String = input.toString
  }
}

object Printable {
  def format[A](input: A)(implicit p: Printable[A]): String = p.format(input)
  def print[A](input: A)(implicit p: Printable[A]): Unit =
    println(format(input))
}

import PrintableInstances._

Printable.print("Hello")

final case class Cat(name: String, age: Int, color: String)
val cat = Cat("Kitty", 2, "foxy")

implicit val catPrintable: Printable[Cat] = new Printable[Cat] {
  def format(input: Cat): String = {
    val name = Printable.format(input.name)
    val age = Printable.format(input.age)
    val color = Printable.format(input.color)
    s"${name} is a ${age} year-old ${color} cat."
  }
}

Printable.print(cat)

object PrintableSyntax {
  implicit class PrintableOps[A](input: A) {
    def format(implicit p: Printable[A]): String = Printable.format(input)
    def print(implicit p: Printable[A]): Unit = Printable.print(input)
  }
}

import PrintableSyntax._

cat.print