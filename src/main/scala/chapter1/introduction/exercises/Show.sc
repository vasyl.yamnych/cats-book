import cats.Show
import cats.instances.int._
import cats.instances.string._

val showInt: Show[Int] = Show.apply[Int]
val showString: Show[String] = Show.apply[String]

val intAsString: String = showInt.show(123)
val stringAsString: String = showString.show("abc")

import cats.syntax.show._

val shownInt = 123.show
val shownString = "abc".show

import java.util.Date

//implicit val dateShow: Show[Date] =
//  new Show[Date] {
//    def show(date: Date): String =
//      s"${date.getTime}ms since the epoch."
//  }

implicit val dateShow: Show[Date] = Show.show(date => s"${date.getTime}ms since the epoch.")

final case class Cat(name: String, age: Int, color: String)
val cat = Cat("Kitty", 2, "foxy")

implicit val catShow: Show[Cat] = Show.show { cat =>
  val name = cat.name.show
  val age = cat.age.show
  val color = cat.color.show
  s"${name} is a ${age} year-old ${color} cat."
}

catShow.show(cat)
cat.show

println(Cat("Garfield", 38, "ginger and black").show)