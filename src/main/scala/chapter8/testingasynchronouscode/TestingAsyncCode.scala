package chapter8.testingasynchronouscode

import scala.concurrent.Future
import scala.language.higherKinds

import cats.Applicative
import cats.Id
import cats.instances.list._
import cats.syntax.traverse._
import cats.syntax.functor._

trait UptimeClient[F[_]] {
  def getUptime(hostname: String): F[Int]
}

class RealUptimeClient extends UptimeClient[Future] {
  def getUptime(hostname: String): Future[Int] = ???
}

class TestUptimeClient(hosts: Map[String, Int]) extends UptimeClient[Id] {
  def getUptime(hostname: String): Int =
    hosts.getOrElse(hostname, 0)
}

class UptimeService[F[_]: Applicative](client: UptimeClient[F]) {
  def getTotalUptime(hostnames: List[String]): F[Int] =
    hostnames.traverse(client.getUptime).map(_.sum)
}

object TestingAsyncCode {
  def testTotalUptime(): Unit = {
    val hosts = Map("hosts1" -> 10, "host2" -> 6)
    val client = new TestUptimeClient(hosts)
    val service = new UptimeService(client)
    val actual = service.getTotalUptime(hosts.keys.toList)
    val expected = hosts.values.sum
    println(s"actual - $actual == expected - $expected")
    assert(actual == expected)
  }

  def example(): Unit = {
    testTotalUptime()
  }
}
