import cats.instances.list._
import cats.syntax.functor._
import cats.syntax.traverse._
import cats.{Applicative, Id}

import scala.concurrent.Future
import scala.language.higherKinds

trait UptimeClient[F[_], A] {
  def getUptime(hostname: String): F[A]
}

trait RealUptimeClient extends UptimeClient[Future, Int] {
  def getUptime(hostname: String): Future[Int]
}

//trait TestUptimeClient extends UptimeClient[Id, Int] {
//  def getUptime(hostname: String): Id[Int]
//}

class TestUptimeClient(hosts: Map[String, Int]) extends UptimeClient[Id, Int] {
  def getUptime(hostname: String): Int =
    hosts.getOrElse(hostname, 0)
}

class UptimeService[F[_]: Applicative, A: Numeric](client: UptimeClient[F, A]) {
  def getTotalUptime(hostnames: List[String]): F[A] =
    hostnames.traverse(client.getUptime).map(_.sum)
}

def testTotalUptime(): Unit = {
  val hosts = Map("host1" -> 10, "host2" -> 6)
  val client = new TestUptimeClient(hosts)
  val service = new UptimeService(client)
  val actual = service.getTotalUptime(hosts.keys.toList)
  val expected = hosts.values.sum
  assert(actual == expected)
}

testTotalUptime()