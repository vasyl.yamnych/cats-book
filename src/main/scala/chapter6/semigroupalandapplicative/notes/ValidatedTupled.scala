package chapter6.semigroupalandapplicative.notes

package object ValidatedTupled {

  import cats.syntax.validated._ // for valid and invalid
  import cats.syntax.apply._ // for tupled
  import cats.instances.string._ // for Semigroup

  val invalidStrings = (
    "Error 1".invalid[Int],
    "Error 2".invalid[Int]
  ).tupled

  import cats.instances.vector._ // for Semigroupal
  val validVectors = (
    Vector(404).invalid[Int],
    Vector(500).invalid[Int]
  ).tupled

  import cats.data.NonEmptyVector
  val invalidNonEmptyVectors = (
    NonEmptyVector.of("Error 1").invalid[Int],
    NonEmptyVector.of("Error 2").invalid[Int]
  ).tupled
}
