import cats.Semigroupal
import cats.data.Validated
import cats.instances.list._
import chapter6.semigroupalandapplicative.notes.ValidatedTupled // for Monoid
type AllErrorsOr[A] = Validated[List[String], A]
Semigroupal[AllErrorsOr].product(
  Validated.invalid(List("Error 1")),
  Validated.invalid(List("Error 2"))
)

val v = Validated.Valid(123)
val i = Validated.Invalid(List("Badness"))

val v1 = Validated.valid[List[String], Int](123)
val i1 = Validated.invalid[List[String], Int](List("Badness"))

import cats.syntax.validated._ // for valid and invalid
123.valid[List[String]]
List("Badness").invalid[Int]

import cats.syntax.applicative._ // for pure
import cats.syntax.applicativeError._ // for raiseError
type ErrorsOr[A] = Validated[List[String], A]
123.pure[ErrorsOr]
List("Badness").raiseError[ErrorsOr, Int]

Validated.catchOnly[NumberFormatException]("foo".toInt)
Validated.catchNonFatal(sys.error("Badness"))
Validated.fromTry(scala.util.Try("foo".toInt))
Validated.fromEither[String, Int](Left("Badness"))
Validated.fromOption[String, Int](None, "Badness")

type AllErrorsOr[A] = Validated[String, A]
import cats.instances.string._ // for Semigroup
Semigroupal[AllErrorsOr]

123.valid.map(_ * 100)
"?".invalid.leftMap(_.toString)
123.valid[String].bimap(_ + "!", _ * 100)
"?".invalid[Int].bimap(_ + "!", _ * 100)

32.valid.andThen { a =>
  10.valid.map { b =>
    a + b
  }
}

import cats.syntax.either._ // for toValidated
"Badness".invalid[Int]
"Badness".invalid[Int].toEither
"Badness".invalid[Int].toEither.toValidated

"fail".invalid[Int].getOrElse(0)
"fail".invalid[Int].fold(_ + "!!!", _.toString)

// ===========================================================================
// Does not work at worksheet, but works if code is at separate object
// ===========================================================================

//import cats.syntax.apply._ // for tupled
//import cats.data.Validated._
//import cats.instances.invariant._ // for Semigroup
//import cats.instances.int._ // for Semigroup
//import cats.instances.tuple._ // for Semigroup
//import cats.syntax.cartesian._
//
//import cats.data.Validated
//import cats.data.Validated._
//import cats.syntax.all._
//import cats.implicits._
//import cats.syntax.functor._ // for map
//import cats.syntax.flatMap._ // for flatMap
//(
//  "Error 1".invalid[Int],
//  "Error 2".invalid[Int]
//).tupled
//
//import cats.instances.vector._ // for Semigroupal
//(
//  Vector(404).invalid[Int],
//  Vector(500).invalid[Int]
//).tupled
//
//import cats.data.NonEmptyVector
//(
//  NonEmptyVector.of("Error 1").invalid[Int],
//  NonEmptyVector.of("Error 2").invalid[Int]
//).tupled

ValidatedTupled.invalidStrings
ValidatedTupled.validVectors
ValidatedTupled.invalidNonEmptyVectors