case class User(name: String, age: Int)

import cats.data.Validated
import chapter6.semigroupalandapplicative.exercises.ValidateUser

type FormData = Map[String, String]
type FailFast[A] = Either[List[String], A]
type FailSlow[A] = Validated[List[String], A]

def getValue(name: String)(data: FormData): FailFast[String] =
    data.get(name).toRight(List(s"$name field is not specified"))

val getName = getValue("name") _
getName(Map("name" -> "Dade Murphy"))
getName(Map())

import cats.syntax.either._ // for catchOnly

type NumFmtExn = NumberFormatException
def parseInt(name: String)(data: String): FailFast[Int] =
    Either
      .catchOnly[NumFmtExn](data.toInt)
      .leftMap(_ => List(s"$name must be an integer"))

parseInt("age")("11")
parseInt("age")("foo")

def nonBlank(name: String)(data: String): FailFast[String] =
    Right(data).ensure(List(s"$name cannot be blank"))(_.nonEmpty)

def nonNegative(name: String)(data: Int): FailFast[Int] =
    Right(data).ensure(List(s"$name must be non-negative"))(_ >= 0)

nonBlank("name")("Dade Murphy")
nonBlank("name")("")

nonNegative("age")(11)
nonNegative("age")(-1)

def readName(data: FormData): FailFast[String] =
    getValue("name")(data).flatMap(nonBlank("name"))

def readAge(data: FormData): FailFast[Int] =
    getValue("age")(data)
      .flatMap(nonBlank("age"))
      .flatMap(parseInt("age"))
      .flatMap(nonNegative("age"))

readName(Map("name" -> "Dade Murphy"))
readName(Map("name" -> ""))
readName(Map())

readAge(Map("age" -> "11"))
readAge(Map("age" -> "-1"))
readAge(Map())


// ====================================================================================
// Does not work at worksheet, but works if code is at separate object
// ====================================================================================
import cats.instances.list._ // for Semigroupal
import cats.syntax.apply._ // for mapN

//def readUser(data: FormData): FailSlow[User] =
//    (
//      readName(data).toValidated,
//      readAge(data).toValidated
//    ).mapN(User.apply)

ValidateUser.readUser(Map("name" -> "Dave", "age" -> "37"))
ValidateUser.readUser(Map("age" -> "-1"))