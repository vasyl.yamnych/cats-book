package chapter6.semigroupalandapplicative.exercises

package object ValidateUser {
  case class User(name: String, age: Int)

  import cats.data.Validated

  type FormData = Map[String, String]
  type FailFast[A] = Either[List[String], A]
  type FailSlow[A] = Validated[List[String], A]

  def getValue(name: String)(data: FormData): FailFast[String] =
    data.get(name).toRight(List(s"$name field is not specified"))

  val getName = getValue("name") _

  import cats.syntax.either._ // for catchOnly

  type NumFmtExn = NumberFormatException
  def parseInt(name: String)(data: String): FailFast[Int] =
    Either
      .catchOnly[NumFmtExn](data.toInt)
      .leftMap(_ => List(s"$name must be an integer"))

  def nonBlank(name: String)(data: String): FailFast[String] =
    Right(data).ensure(List(s"$name cannot be blank"))(_.nonEmpty)

  def nonNegative(name: String)(data: Int): FailFast[Int] =
    Right(data).ensure(List(s"$name must be non-negative"))(_ >= 0)

  def readName(data: FormData): FailFast[String] =
    getValue("name")(data).flatMap(nonBlank("name"))

  def readAge(data: FormData): FailFast[Int] =
    getValue("age")(data)
      .flatMap(nonBlank("age"))
      .flatMap(parseInt("age"))
      .flatMap(nonNegative("age"))

  import cats.instances.list._ // for Semigroupal
  import cats.syntax.apply._ // for mapN

  def readUser(data: FormData): FailSlow[User] =
    (
      readName(data).toValidated,
      readAge(data).toValidated
    ).mapN(User.apply)
}
