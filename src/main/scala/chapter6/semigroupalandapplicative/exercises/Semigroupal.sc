import cats.Monad
import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap

def product[M[_]: Monad, A, B](x: M[A], y: M[B]): M[(A, B)] = {
  x.flatMap(a => y.map(b => (a, b)))
}

def product2[M[_]: Monad, A, B](x: M[A], y: M[B]): M[(A, B)] =
  for {
    a <- x
    b <- y
  } yield (a, b)

import cats.instances.list._ // for Semigroupal
product(List(1, 2), List(3, 4))
product2(List(1, 2), List(3, 4))

import cats.instances.either._ // for Semigroupal
type ErrorOr[A] = Either[Vector[String], A]
product[ErrorOr, Int, Int](
  Left(Vector("Error 1")),
  Left(Vector("Error 2"))
)
product2[ErrorOr, Int, Int](
  Left(Vector("Error 1")),
  Left(Vector("Error 2"))
)