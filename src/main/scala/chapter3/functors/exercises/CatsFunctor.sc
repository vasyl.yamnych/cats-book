sealed trait Tree[+A]
final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A](value: A) extends Tree[A]

val tree1 = Branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4))
val tree2 = Leaf(5)
val tree1t: Tree[Int] = tree1
val tree2t: Tree[Int] = tree2

import cats.Functor
import cats.syntax.functor._
import scala.language.higherKinds

implicit def treeFunctor[F[_]] = new Functor[Tree] {
  def map[A, B](fa: Tree[A])(f: A => B): Tree[B] = fa match {
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))
    case Leaf(v)      => Leaf(f(v))
  }
}

//tree1 map (_ + 100)
//tree2 map (_ + 100)
tree1t map (_ + 100)
tree2t map (_ + 100)

object Tree {
  def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)
  def leaf[A](value: A): Tree[A] = Leaf(value)
}

import Tree._

val tree3 = branch(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))), Leaf(4))
val tree4 = leaf(5)

tree3 map (_ + 100)
tree4 map (_ + 100)