import cats.Functor
import cats.instances.function._ // for Functor
import cats.syntax.functor._ // for map

val func1 = (x: Int) => x.toDouble + 1
val func2 = (y: Double) => y * 2
val func3 = func1.map(func2)

import cats.instances.either._ // for Functor
val either: Either[String, Int] = Right(123)
either.map(_ + 1)

val func3a: Int => Double = a => func2(func1(a))
val func3b: Int => Double = func2.compose(func1)

import cats.syntax.contravariant._ // for contramap
// Hypothetical example. This won't actually compile:
//val func3c: Int => Double = func2.contramap(func1)

type <=[B, A] = A => B
type F[A] = Double <= A

val func2b: Double <= Double = func2
val func3c = func2b.contramap(func1)

func2(3)
func2b(3)
func3c(3)