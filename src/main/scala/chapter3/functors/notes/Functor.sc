List(1, 2, 3).map(n => n + 1).map(n => n * 2).map(n => n + "!")

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
val future: Future[String] =
  Future(123).map(n => n + 1).map(n => n * 2).map(n => n + "!")
Await.result(future, 1.second)

import scala.util.Random
val future1 = {
  // Initialize Random with a fixed seed:
  val r = new Random(0L)
  // nextInt has the side-effect of moving to
  // the next random number in the sequence:
  val x = Future(r.nextInt)
  for {
    a <- x
    b <- x
  } yield (a, b)
}

val future2 = {
  val r = new Random(0L)
  for {
    a <- Future(r.nextInt)
    b <- Future(r.nextInt)
  } yield (a, b)
}

val result1 = Await.result(future1, 1.second)
val result2 = Await.result(future2, 1.second)

import cats.instances.function._ // for Functor
import cats.syntax.functor._ // for map

val func1: Int => Double = (x: Int) => x.toDouble
val func2: Double => Double = (y: Double) => y * 2

(func1 map func2)(1) // composition using map
(func1 andThen func2)(1) // composition using andThen
func2(func1(1)) // composition written out by hand

val func =
  ((x: Int) => x.toDouble).
    map(x => x + 1).
    map(x => x * 2).
    map(x => x + "!")

func(123)

func1.map(func2)(1)
