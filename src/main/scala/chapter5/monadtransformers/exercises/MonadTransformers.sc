import cats.data.EitherT

import scala.concurrent.{Await, Future}

type Response[A] = EitherT[Future, String, A]

val powerLevels = Map(
  "Jazz" -> 6,
  "Bumblebee" -> 8,
  "Hot Rod" -> 10
)

import cats.instances.future._ // for Monad
import cats.syntax.flatMap._ // for flatMap
import scala.concurrent.ExecutionContext.Implicits.global

def getPowerLevel(ally: String): Response[Int] = {
  powerLevels.get(ally) match {
    case Some(avg) => EitherT.right(Future(avg))
    case None      => EitherT.left(Future(s"$ally unreachable"))
  }
}

def canSpecialMove(ally1: String, ally2: String): Response[Boolean] = {
  val l1 = getPowerLevel(ally1)
  val l2 = getPowerLevel(ally2)
  l1.flatMap(v1 => l2.map(_ + v1 > 15))
}

import scala.concurrent.duration._

def tacticalReport(ally1: String, ally2: String): String = {
  val res = canSpecialMove(ally1, ally2).fold(
    msg => s"Comms error: $msg",
    b =>
    if (b) s"$ally1 and $ally2 are ready to roll out!"
    else s"$ally1 and $ally2 need a recharge.")
  Await.result(res, 5.second)
}

tacticalReport("Jazz", "Bumblebee")
tacticalReport("Bumblebee", "Hot Rod")
tacticalReport("Jazz", "Ironhide")