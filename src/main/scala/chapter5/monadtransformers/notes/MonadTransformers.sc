import cats.data.{EitherT, OptionT}
type ListOption[A] = OptionT[List, A]

import cats.Monad
import cats.instances.list._ // for Monad
import cats.syntax.applicative._ // for pure

val result1: ListOption[Int] = OptionT(List(Option(10)))
val result2: ListOption[Int] = 32.pure[ListOption]

result1.flatMap { (x: Int) =>
  result2.map { (y: Int) => x + y
  }
}

type ErrorOr[A] = Either[String, A]
type ErrorOrOption[A] = OptionT[ErrorOr, A]

import cats.instances.either._ // for Monad
val a = 10.pure[ErrorOrOption]
val b = 32.pure[ErrorOrOption]
val c = a.flatMap(x => b.map(y => x + y))

val errorStack1 = OptionT[ErrorOr, Int](Right(Some(10)))
val errorStack2 = 32.pure[ErrorOrOption]
errorStack1.value
errorStack2.value.map(_.getOrElse(-1))

import cats.data.Writer
type Logged[A] = Writer[List[String], A]

def parseNumber(str: String): Logged[Option[Int]] =
  util.Try(str.toInt).toOption match {
    case Some(num) => Writer(List(s"Read $str"), Some(num))
    case None      => Writer(List(s"Failed on $str"), None)
  }

def addAll(a: String, b: String, c: String): Logged[Option[Int]] = {
  import cats.data.OptionT
  val result = for {
    a <- OptionT(parseNumber(a))
    b <- OptionT(parseNumber(b))
    c <- OptionT(parseNumber(c))
  } yield a + b + c
  result.value
}

val result3 = addAll("1", "2", "3")
val result4 = addAll("1", "a", "3")