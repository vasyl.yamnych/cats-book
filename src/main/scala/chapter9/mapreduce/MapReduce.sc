import cats.kernel.Monoid
import cats.instances.int._ // for Monoid
import cats.instances.string._ // for Monoid
import cats.syntax.semigroup._ // for |+|

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

def foldMap[A, B: Monoid](values: Vector[A])(func: A => B): B =
    values.map(func).foldLeft(Monoid[B].empty)(_ |+| _)

foldMap(Vector(1, 2, 3))(identity)
foldMap(Vector(1, 2, 3))(_.toString + "! ")
foldMap("Hello world!".toVector)(_.toString.toUpperCase)

def foldMap1[A, B: Monoid](as: Vector[A])(func: A => B): B =
    as.foldLeft(Monoid[B].empty)(_ |+| func(_))

foldMap1(Vector(1, 2, 3))(identity)
foldMap1(Vector(1, 2, 3))(_.toString + "! ")
foldMap1("Hello world!".toVector)(_.toString.toUpperCase)

Runtime.getRuntime.availableProcessors
(1 to 10).toList.grouped(3).toList

def parallelFoldMap[A, B: Monoid](values: Vector[A])(
      func: A => B): Future[B] = {
    val size = values.size / Runtime.getRuntime.availableProcessors.ceil.toInt
    val futures = values.grouped(size).map(g => Future(foldMap1(g)(func)))
    Future.sequence(futures).map { iterable =>
      iterable.foldLeft(Monoid[B].empty)(_ |+| _)
    }
  }

val result: Future[Int] = parallelFoldMap((1 to 1000000).toVector)(identity)
Await.result(result, 1.second)

import cats.Monoid
import cats.Foldable
import cats.Traverse
import cats.instances.int._ // for Monoid
import cats.instances.future._ // for Applicative and Monad
import cats.instances.vector._ // for Foldable and Traverse
import cats.syntax.semigroup._ // for |+|
import cats.syntax.foldable._ // for combineAll and foldMap
import cats.syntax.traverse._ // for traverse

def parallelFoldMap1[A, B: Monoid](values: Vector[A])(
      func: A => B): Future[B] = {
    val numCores = Runtime.getRuntime.availableProcessors
    val groupSize = (1.0 * values.size / numCores).ceil.toInt
    values
      .grouped(groupSize)
      .toVector
      .traverse(group => Future(group.toVector.foldMap(func)))
      .map(_.combineAll)
  }

val future: Future[Int] = parallelFoldMap1((1 to 1000).toVector)(_ * 1000)
Await.result(future, 1.second)