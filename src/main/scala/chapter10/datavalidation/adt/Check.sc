import chapter10.datavalidation.adt.{Check, Pure}

import cats.syntax.either._ // for asLeft and asRight
import cats.instances.list._ // for Semigroup
val a: Check[List[String], Int] =
  Pure { v =>
    if(v > 2) v.asRight
    else List("Must be > 2").asLeft
  }
val b: Check[List[String], Int] =
  Pure { v =>
    if(v < -2) v.asRight
    else List("Must be < -2").asLeft
  }
val check: Check[List[String], Int] =
  a and b
check(5)
check(0)
