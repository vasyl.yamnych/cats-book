package chapter10.datavalidation.either

import cats.Semigroup
import cats.syntax.apply._ // for mapN
import cats.instances.either._

sealed trait Check[E, A] {
  def and(that: Check[E, A]): Check[E, A] =
    And(this, that)
  def apply(a: A)(implicit s: Semigroup[E]): Either[E, A] =
    this match {
      case Pure(func)       => func(a)
      case And(left, right) =>
        //left(a).product(right(a).map( _ => a)
        (left(a), right(a)).mapN((b, _) => b)
    }
}

final case class And[E, A](left: Check[E, A], right: Check[E, A])
    extends Check[E, A]
final case class Pure[E, A](func: A => Either[E, A]) extends Check[E, A]
