import cats.instances.list._
import cats.syntax.either._
import chapter10.datavalidation.either.{Check, Pure}

val a: Check[List[String], Int] =
  Pure { v =>
    if(v > 2) v.asRight
    else List("Must be > 2").asLeft
  }

val b: Check[List[String], Int] =
  Pure { v =>
    if(v < -2) v.asRight
    else List("Must be < -2").asLeft
  }

val check: Check[List[String], Int] =
  a and b

check(5)
check(0)

