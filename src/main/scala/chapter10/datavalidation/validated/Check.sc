import cats.syntax.validated._
import cats.instances.list._
import chapter10.datavalidation.validated._

val a: Check[List[String], Int] =
  Pure { v =>
    if(v > 2) v.valid
    else List("Must be > 2").invalid
  }

val b: Check[List[String], Int] =
  Pure { v =>
    if(v < -2) v.valid
    else List("Must be < -2").invalid
  }

val check: Check[List[String], Int] = a and b

check(5)
check(0)

//TODO issues with worksheet
//def check2: Check[List[String], Int] = a or b
//
//check2(5)
//check2(1)


