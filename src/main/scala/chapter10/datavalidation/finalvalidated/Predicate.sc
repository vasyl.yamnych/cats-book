import cats.instances.list._
import cats.syntax.validated._
import chapter10.datavalidation.finalvalidated.Predicate
import chapter10.datavalidation.finalvalidated.Predicate._

val a: Predicate[List[String], Int] =
  Pure { v =>
    if(v > 2) v.valid
    else List("Must be > 2").invalid
  }

val b: Predicate[List[String], Int] =
  Pure { v =>
    if(v < -2) v.valid
    else List("Must be < -2").invalid
  }

val predicate: Predicate[List[String], Int] = a and b
predicate(5)
predicate(0)

val predicate2: Predicate[List[String], Int] = a or b
predicate2(5)
predicate2(1)
