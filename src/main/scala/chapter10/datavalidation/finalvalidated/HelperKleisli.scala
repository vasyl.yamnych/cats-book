package chapter10.datavalidation.finalvalidated

import cats.data.{Kleisli, NonEmptyList, Validated}
import cats.instances.either._ // for Semigroupal
import cats.instances.list._ // for Monad
import cats.syntax.apply._

object HelperKleisli {

  type Errors = NonEmptyList[String]
  def error(s: String): NonEmptyList[String] = NonEmptyList(s, Nil)

  type Result[A] = Either[Errors, A]
  type Check[A, B] = Kleisli[Result, A, B]

  // Create a check from a function:
  def check[A, B](func: A => Result[B]): Check[A, B] =
    Kleisli(func)

  // Create a check from a Predicate:
  def checkPred[A](pred: Predicate[Errors, A]): Check[A, A] =
    Kleisli[Result, A, A](pred.run)

  def longerThan(n: Int): Predicate[Errors, String] =
    Predicate.lift(error(s"Must be longer than $n characters"), str => str.size > n)
  val alphanumeric: Predicate[Errors, String] =
    Predicate.lift(error(s"Must be all alphanumeric characters"), str => str.forall(_.isLetterOrDigit))
  def contains(char: Char): Predicate[Errors, String] =
    Predicate.lift(error(s"Must contain the character $char"), str => str.contains(char))
  def containsOnce(char: Char): Predicate[Errors, String] =
    Predicate.lift(error(s"Must contain the character $char only once"), str => str.filter(c => c == char).size == 1)

  final case class User(username: String, email: String)

  // A username must contain at least four characters
  // and consist entirely of alphanumeric characters
  val checkUsername: Check[String, String] = checkPred(longerThan(3) and alphanumeric)

  // An email address must contain a single `@` sign.
  // Split the string at the `@`.
  // The string to the left must not be empty.
  // The string to the right must be
  // at least three characters long and contain a dot.
  val splitEmail: Check[String, (String, String)] = check(_.split('@') match {
    case Array(name, domain) => Right((name, domain))
    case other               => Left(error("Must contain a single @ character"))
  })

  val checkLeft: Check[String, String] = checkPred(longerThan(0))
  val checkRight: Check[String, String] = checkPred(longerThan(3) and contains('.'))
  val joinEmail: Check[(String, String), String] = check {
    case (l, r) => (checkLeft(l), checkRight(r)).mapN(_ + "@" + _)
  }
  val checkEmail: Check[String, String] = splitEmail andThen joinEmail

  def createUser(username: String, email: String): Either[Errors, User] =
    (checkUsername.run(username), checkEmail.run(email)).mapN(User)
}
