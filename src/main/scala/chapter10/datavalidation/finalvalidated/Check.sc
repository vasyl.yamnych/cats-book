import chapter10.datavalidation.finalvalidated.Helper._
import chapter10.datavalidation.finalvalidated.HelperKleisli

createUser("Noel", "noel@underscore.io")
createUser("", "dave@underscore@io")

HelperKleisli.createUser("Noel", "noel@underscore.io")
HelperKleisli.createUser("", "dave@underscore@io")
