import cats.data.Kleisli
import cats.instances.list._ // for Monad

val step1: Kleisli[List, Int, Int] = Kleisli(x => List(x + 1, x - 1))
val step2: Kleisli[List, Int, Int] = Kleisli(x => List(x, -x))
val step3: Kleisli[List, Int, Int] = Kleisli(x => List(x * 2, x / 2))
val step4: Kleisli[List, Int, Int] = Kleisli(x => List(x * 10))

val pipeline = step1 andThen step2 andThen step3

(pipeline andThen step4).run(20)
pipeline.run(20)
(step1 andThen step2).run(20)
step1.run(20)