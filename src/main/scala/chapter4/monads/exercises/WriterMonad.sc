def slowly[A](body: => A): A =
  try body
  finally Thread.sleep(100)

def factorialPrint(n: Int): Int = {
  val ans = slowly(if (n == 0) 1 else n * factorialPrint(n - 1))
  println(s"fact $n $ans")
  ans
}

factorialPrint(5)

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

Await.result(Future.sequence(
               Vector(
                 Future(factorialPrint(3)),
                 Future(factorialPrint(3))
               )),
             5.seconds)

import cats.data.Writer
import cats.instances.vector._ // for Monoid
import cats.syntax.applicative._ // for pure

type Logged[A] = Writer[Vector[String], A]
42.pure[Logged]

import cats.syntax.writer._ // for tell
Vector("Message").tell

41.pure[Logged].map(_ + 1)

def factorial(n: Int): Logged[Int] =
  for {
    ans <- if (n == 0) {
      1.pure[Logged]
    } else {
      factorial(n - 1).map(_ * n)
    }
    _ <- Vector(s"fact $n $ans").tell
  } yield ans

val (log, res) = factorial(9).run

val Vector((logA, ansA), (logB, ansB)) =
  Await.result(Future.sequence(Vector(
    Future(factorial(3).run),
    Future(factorial(5).run)
  )), 5.seconds)