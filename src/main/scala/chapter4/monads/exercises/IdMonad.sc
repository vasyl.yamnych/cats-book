import cats.Id

def pure[A](v: A): Id[A] = v
def map[A, B](fa: Id[A])(f: A => B): Id[B] = f(fa)
def flatMap[A, B](fa: Id[A])(f: A => Id[B]): Id[B] = f(fa)

pure(123)
map(123)(_ * 2)
flatMap(123)(_ * 2)