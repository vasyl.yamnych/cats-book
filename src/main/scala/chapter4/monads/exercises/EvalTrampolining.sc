import cats.Eval

def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): Eval[B] =
  as match {
    case head :: tail =>
      Eval.defer(foldRight(tail, acc)(fn).map(r => fn(head, r)))
    case Nil =>
      Eval.now(acc)
  }

foldRight((1 to 100000).toList, 0L)(_ + _).value

def foldRightEval[A, B](as: List[A], acc: Eval[B])
                       (fn: (A, Eval[B]) => Eval[B]): Eval[B] =
  as match {
    case head :: tail =>
      Eval.defer(fn(head, foldRightEval(tail, acc)(fn)))
    case Nil =>
      acc
  }

def foldRightBook[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
  foldRightEval(as, Eval.now(acc)) { (a, b) =>
    b.map(fn(a, _))
  }.value

foldRightBook((1 to 100000).toList, 0L)(_ + _)

def foldRightUnsafe[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
  as match {
    case head :: tail =>
      fn(head, foldRightUnsafe(tail, acc)(fn))
    case Nil =>
      acc
  }

foldRightUnsafe((1 to 100000).toList, 0L)(_ + _)