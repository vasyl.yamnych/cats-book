sealed trait Tree[+A]
final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A](value: A) extends Tree[A]
def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)
def leaf[A](value: A): Tree[A] = Leaf(value)

import cats.Monad

implicit val treeMonad: Monad[Tree] = new Monad[Tree] {
  def flatMap[A, B](tree: Tree[A])(fn: A => Tree[B]): Tree[B] =
    tree match {
      case Branch(l, r) => Branch(flatMap(l)(fn), flatMap(r)(fn))
      case Leaf(v)      => fn(v)
    }

  def pure[A](v: A): Tree[A] = leaf(v)

  def tailRecM[A, B](a: A)(func: A => Tree[Either[A, B]]): Tree[B] =
    flatMap(func(a)) {
      case Left(value) =>
        tailRecM(value)(func)
      case Right(value) =>
        Leaf(value)
    }
}

import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap

val b = branch(leaf(100), leaf(200))
val l0 = leaf(100)
b.flatMap(x => branch(leaf(x - 1), leaf(x + 1)))
l0.flatMap(x => branch(leaf(x - 1), leaf(x + 1)))

for {
  a <- branch(leaf(100), leaf(200))
  b <- branch(leaf(a - 10), leaf(a + 10))
  c <- branch(leaf(b - 1), leaf(b + 1))
} yield c