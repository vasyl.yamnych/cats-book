import scala.language.higherKinds
import cats.Monad
import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap

def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
  for {
    x <- a
    y <- b
  } yield x * x + y * y

//sumSquare(3, 4)

import cats.Id
sumSquare(3: Id[Int], 4: Id[Int])

"Dave": Id[String]
123: Id[Int]
List(1, 2, 3): Id[List[Int]]

val a = Monad[Id].pure(3)
val b = Monad[Id].flatMap(a)(_ + 1)

import cats.syntax.functor._ // for map
import cats.syntax.flatMap._ // for flatMap
for {
  x <- a
  y <- b
} yield x + y
